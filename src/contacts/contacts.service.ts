import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Contact } from './contact.entity';
import { User } from '../users/user.entity';

@Injectable()
export class ContactsService {
  constructor(
    @InjectRepository(Contact) private contactRepository: Repository<Contact>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async saveContact(contact: Contact, user: User) {
    const newContact = await this.contactRepository.save(contact);
    const updatedUser = this.userRepository.create({
      ...user,
      contacts: [...user.contacts, newContact],
    });
    await this.userRepository.save(updatedUser);
    return newContact;
  }

  async updateContact(editContact: Contact) {
    await getConnection()
      .createQueryBuilder()
      .update(Contact)
      .set({
        contactName: editContact.contactName,
        contactAddress: editContact.contactAddress,
        contactEmail: editContact.contactEmail,
      })
      .where('id = :id', { id: editContact.id })
      .execute();
    return editContact;
  }

  async removeContact(contact: Contact) {
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Contact)
      .where('id = :id', { id: contact.id })
      .execute();
    return contact;
  }

  async create(contact: Contact, user: User) {
    return this.saveContact(contact, user);
  }

  async update(contact: Contact) {
    return this.updateContact(contact);
  }

  async remove(contact: Contact) {
    await this.removeContact(contact);
  }
  async getContactsForUser(id: string | number) {
    const user = await this.userRepository.findOne(id, {
      relations: ['contacts'],
    });
    const userContacts = this.userRepository.create({
      contacts: [...user.contacts],
    });
    const { contacts } = userContacts;

    return contacts;
  }
}

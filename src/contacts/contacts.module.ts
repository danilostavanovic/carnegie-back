import { Module } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { ContactsController } from './contacts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Contact } from './contact.entity';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';

@Module({
  controllers: [ContactsController],
  providers: [ContactsService, UsersService],
  imports: [TypeOrmModule.forFeature([Contact]), UsersModule],
  exports: [TypeOrmModule],
})
export class ContactsModule {}

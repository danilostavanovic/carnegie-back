import { Controller, Post, Body, Put, Param, Get } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { Contact } from './contact.entity';
import { UsersService } from '../users/users.service';

@Controller('contacts')
export class ContactsController {
  constructor(
    private readonly contactsService: ContactsService,
    private userService: UsersService,
  ) {}

  @Post('new/:id')
  async create(@Body() contact: Contact, @Param() { id }) {
    const user = await this.userService.getUserById(id);
    return await this.contactsService.create(contact, user);
  }

  @Put('update')
  async update(@Body() contact: Contact) {
    return await this.contactsService.update(contact);
  }

  @Post('delete')
  async remove(@Body() contact: Contact) {
    return await this.contactsService.remove(contact);
  }
  @Get(':id')
  async getContacts(@Param() { id }) {
    return this.contactsService.getContactsForUser(id);
  }
}

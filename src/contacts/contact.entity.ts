import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from '../users/user.entity';

@Entity('contact')
export class Contact {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 25 })
  contactName: string;

  @Column({ length: 25 })
  contactEmail: string;

  @Column({ length: 25 })
  contactAddress: string;

  @ManyToOne((type) => User, (user) => user.contacts, {
    cascade: true,
  })
  ownerContacts: User;
}

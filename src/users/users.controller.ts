import {
  Controller,
  Post,
  Body,
  Get,
  Put,
  Delete,
  Param,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './user.entity';

@Controller('users')
export class UsersController {
  constructor(private service: UsersService) {}

  @Get('all')
  getUsers(@Param() params) {
    return this.service.getUsers();
  }

  @Post('auth')
  async get(@Body() user: User) {
    const findUser = await this.service.getUserAuth(user);
    if (findUser.length > 0) {
      return findUser[0];
    }
    throw new HttpException(
      {
        status: HttpStatus.NOT_FOUND,
        error: `User with username ${user.username} does not exist, or wrong credentials`,
      },
      HttpStatus.NOT_FOUND,
    );
  }

  @Post('new')
  async create(@Body() user: User) {
    if (await this.service.doesUserExists(user)) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: `User with '${user.username}' username already  exists`,
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return this.service.createUser(user);
  }

  @Put()
  update(@Body() user: User) {
    return this.service.updateUser(user);
  }

  @Delete(':id')
  deleteUser(@Param() params) {
    return this.service.deleteUser(params.id);
  }
}

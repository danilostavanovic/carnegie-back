import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Contact } from '../contacts/contact.entity';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 25 })
  fullName: string;

  @Column({ length: 15 })
  username: string;

  @Column({ length: 15 })
  password: string;

  @OneToMany((type) => Contact, (contact) => contact.ownerContacts)
  contacts: Contact[];
}

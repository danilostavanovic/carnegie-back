import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  getUsers(): Promise<User[]> {
    return this.usersRepository.find({
      relations: ['contacts'],
    });
  }

  async getUserById(id: string): Promise<User> {
    return await this.usersRepository.findOne(id, {
      relations: ['contacts'],
    });
  }

  getUserAuth(user: User): Promise<User[]> {
    return this.usersRepository.find({
      select: ['fullName', 'username', 'id'],
      where: [{ username: user.username, password: user.password }],
    });
  }

  async updateUser(user: User) {
    await this.usersRepository.save(user);
  }

  async deleteUser(user: User) {
    await this.usersRepository.delete(user);
  }

  async doesUserExists(userEntity: User) {
    const user = await this.usersRepository.find({
      where: [{ username: userEntity.username }],
    });
    return user.length != 0;
  }

  async createUser(user: User) {
    await this.usersRepository.save(user);
  }
}
